package by.softclub.pro.api.repository;

import by.softclub.pro.api.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameEquals(String username);
}
