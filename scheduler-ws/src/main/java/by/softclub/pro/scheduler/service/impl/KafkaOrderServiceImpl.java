package by.softclub.pro.scheduler.service.impl;

import by.softclub.pro.scheduler.dto.OrderClaimDto;
import by.softclub.pro.scheduler.service.MailService;
import by.softclub.pro.scheduler.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class KafkaOrderServiceImpl {

    @Autowired
    private OrderService orderService;

    @Autowired
    private MailService mailService;

    @KafkaListener(id = "1234", topics = {"order-claim"}, containerFactory = "singleFactory")
    public void consume(OrderClaimDto dto) {
        orderService.save(dto);
//        mailService.sendMessage(dto);
    }
}