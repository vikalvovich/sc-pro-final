package by.softclub.pro.api.exception;

public class PasswordException extends RuntimeException {

    public static String error = "ERROR_IN_PASSWORD";

    public PasswordException(String userError) {
        super(userError);
    }

    public PasswordException() {
        super(error);
    }
}
