package by.softclub.pro.api.dto;

import by.softclub.pro.api.validator.OrderConstraint;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@OrderConstraint
public class OrderDto{
    @NotNull
    private String sourceCurrency;
    @NotNull
    private String targetCurrency;
    @NotNull
    private BigDecimal amount;
}
