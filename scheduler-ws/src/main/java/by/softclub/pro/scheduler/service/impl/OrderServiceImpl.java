package by.softclub.pro.scheduler.service.impl;

import by.softclub.pro.scheduler.domain.Order;
import by.softclub.pro.scheduler.dto.OrderClaimDto;
import by.softclub.pro.scheduler.repository.CurrencyRepository;
import by.softclub.pro.scheduler.repository.OrderRepository;
import by.softclub.pro.scheduler.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@CacheConfig(cacheNames = "orders")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public void save(OrderClaimDto dto) {
        var order = new Order();

        order.setAmount(dto.getAmount());

        currencyRepository.findByCode(dto.getSourceCurrency()).ifPresent(
                order::setSourceCurrency
        );

        currencyRepository.findByCode(dto.getTargetCurrency()).ifPresent(
                order::setTargetCurrency
        );

        orderRepository.save(order);
    }

}
