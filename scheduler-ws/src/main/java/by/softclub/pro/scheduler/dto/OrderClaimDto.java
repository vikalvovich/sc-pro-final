package by.softclub.pro.scheduler.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
public class OrderClaimDto {

    private String sourceCurrency;
    private String targetCurrency;
    private BigDecimal amount;
    private String email;

    @Override
    public String toString() {
        return "OrderClaimDto{" +
                "sourceCurrency='" + sourceCurrency + '\'' +
                ", targetCurrency='" + targetCurrency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
