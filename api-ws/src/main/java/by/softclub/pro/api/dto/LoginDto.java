package by.softclub.pro.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
public class LoginDto{
    @NotNull
    @NotEmpty(message = "Login is empty")
    private String login;
    @NotNull
    @NotEmpty(message = "Password is empty")
    private String password;
}
