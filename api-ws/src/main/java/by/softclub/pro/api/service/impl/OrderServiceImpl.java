package by.softclub.pro.api.service.impl;

import by.softclub.pro.api.domain.User;
import by.softclub.pro.api.dto.OrderClaimDto;
import by.softclub.pro.api.dto.OrderDto;
import by.softclub.pro.api.repository.OrderRepository;
import by.softclub.pro.api.service.KafkaSendService;
import by.softclub.pro.api.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private KafkaSendService kafkaSendService;

    @Override
    public List<OrderDto> findAll() {
        return orderRepository.findAll().stream()
                .map(order -> {
                    var dto = new OrderDto();
                    dto.setAmount(order.getAmount());
                    dto.setSourceCurrency(order.getSourceCurrency().getCode());
                    dto.setTargetCurrency(order.getTargetCurrency().getCode());
                    return dto;
                }).collect(Collectors.toList());
    }

    @Override
    public void send(OrderDto dto) {
        OrderClaimDto orderClaimDto = new OrderClaimDto();
        ModelMapper mapper = new ModelMapper();
        mapper.map(dto, orderClaimDto);
        orderClaimDto.setEmail(((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
        kafkaSendService.send(orderClaimDto);
    }

}
