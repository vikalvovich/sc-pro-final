package by.softclub.pro.api.service;

import by.softclub.pro.api.dto.OrderDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> findAll();

    void send(OrderDto dto);
}
