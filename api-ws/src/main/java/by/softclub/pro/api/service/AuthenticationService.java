package by.softclub.pro.api.service;

import by.softclub.pro.api.domain.User;
import by.softclub.pro.api.dto.LoginDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {
    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);
}
