package by.softclub.pro.api.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderClaimDto extends OrderDto {

    private String email;
}
