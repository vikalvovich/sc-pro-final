package by.softclub.pro.scheduler.service;

import by.softclub.pro.scheduler.dto.OrderClaimDto;

public interface MailService {

    void sendMessage(OrderClaimDto dto);
}
