package by.softclub.pro.api.service;

import by.softclub.pro.api.dto.OrderClaimDto;

public interface KafkaSendService {
    void send(OrderClaimDto dto);
}
