package by.softclub.pro.api.validator;

import by.softclub.pro.api.dto.OrderDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class OrderValidator implements ConstraintValidator<OrderConstraint, OrderDto> {

    private String message;

    @Override
    public void initialize(OrderConstraint constraintAnnotation) {
        this.message = "ERROR_ORDER";
    }

    @Override
    public boolean isValid(OrderDto orderDto, ConstraintValidatorContext constraintValidatorContext) {

        boolean isValid = true;

        if (orderDto.getAmount() != null) {
            if (orderDto.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
                isValid = false;
                constraintValidatorContext
                        .buildConstraintViolationWithTemplate(this.message)
                        .addConstraintViolation();
            }
        }
        return isValid;
    }
}