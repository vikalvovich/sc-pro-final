package by.softclub.pro.api.controller;

import by.softclub.pro.api.domain.User;
import by.softclub.pro.api.dto.LoginDto;
import by.softclub.pro.api.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(value = "/login")
    public String login(@Valid @RequestBody LoginDto dto) {
        User user = (User) authenticationService.loadUserByUsername(dto.getLogin());
        return authenticationService.login(user, dto);
    }
}
