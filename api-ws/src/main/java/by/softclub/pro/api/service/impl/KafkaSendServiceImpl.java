package by.softclub.pro.api.service.impl;

import by.softclub.pro.api.dto.OrderClaimDto;
import by.softclub.pro.api.service.KafkaSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSendServiceImpl implements KafkaSendService {

    @Autowired
    private KafkaTemplate<Long, OrderClaimDto> kafkaTemplate;

    @Override
    public void send(OrderClaimDto dto) {
        kafkaTemplate.send("order-claim", dto);
    }
}
