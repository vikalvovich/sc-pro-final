package by.softclub.pro.api.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = OrderValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderConstraint {
    String message() default "ERROR_ORDER";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
