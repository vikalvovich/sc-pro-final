package by.softclub.pro.scheduler.repository;

import by.softclub.pro.scheduler.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
