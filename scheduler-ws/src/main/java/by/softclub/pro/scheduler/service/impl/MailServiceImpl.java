package by.softclub.pro.scheduler.service.impl;

import by.softclub.pro.scheduler.dto.OrderClaimDto;
import by.softclub.pro.scheduler.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendMessage(OrderClaimDto dto) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(dto.getEmail());
        message.setSubject("Order created");
        message.setText(dto.toString());
        emailSender.send(message);
    }
}
